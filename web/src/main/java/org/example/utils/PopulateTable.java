package org.example.utils;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.example.entities.CatEntity;
import org.example.repositories.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@RequiredArgsConstructor
public class PopulateTable {

    private final CatRepository catRepository;

   /* public PopulateTable(CatRepository catRepository){
        this.catRepository = catRepository;
    }*/

    @PostConstruct
    public void init() {
        var cat = new CatEntity();
        var cat1 = new CatEntity();
        var cat2 = new CatEntity();
        var cat3 = new CatEntity();
        var cat4 = new CatEntity();
        var cat5 = new CatEntity();

        cat.setAge(0);
        cat.setName("test 1");

        cat1.setName("test2");
        cat1.setAge(1);

        cat2.setName("test3");
        cat2.setAge(32);

        cat3.setName("test3");
        cat3.setAge(5);

        cat4.setName("test3");
        cat4.setAge(6);

        cat5.setName("test3");
        cat5.setAge(10);

        var l = new ArrayList<CatEntity>();

        l.add(cat);
        l.add(cat1);
        l.add(cat2);
        l.add(cat3);
        l.add(cat4);
        l.add(cat5);

        catRepository.saveAll(l);

    }
}
