package org.example.controller;

import org.example.entities.CatEntity;
import org.example.repositories.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cat")
@CrossOrigin("http://localhost:4200")
public class CatController {
    @Autowired
    private CatRepository catRepository;

    @GetMapping
    public List<CatEntity> getCats() {
        return this.catRepository.findAll();
    }

    @DeleteMapping("/{id}")
    public void deleteCat(@PathVariable("id") Long id) {
        this.catRepository.deleteById(id);
    }

    @PutMapping("/{id}")
    public CatEntity updateCatById(@PathVariable("id") Long id){
        //@TODO complete

        return null;
    }

    @PutMapping("/update")
    public CatEntity updateCatByEntity(@RequestBody CatEntity entity) {
        return this.catRepository.save(entity);
    }

    @PostMapping("/add")
    public CatEntity addANewCat(@RequestBody CatEntity newCat) {
        return this.catRepository.save(newCat);
    }
}
