import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CatDto} from "../cat-dto";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CatService {

  constructor(private httpClient : HttpClient) { }

  addNewCat(data : CatDto) : Observable<CatDto> {
    return this.httpClient.post<CatDto>('http://localhost:8080/api/cat/add', data);
  }

  getAllCats() : Observable<CatDto[]> {
    return this.httpClient.get<CatDto[]>('http://localhost:8080/api/cat');
  }

  updateCatById(idCat : number) : Observable<CatDto> {
    return this.httpClient.put<CatDto>('http://localhost:8080/api/cat/' + idCat, {});
  }

  updateCatByEntity(catToUpdate : CatDto) : Observable<CatDto> {
    return this.httpClient.put<CatDto>('http://localhost:8080/api/cat/update', catToUpdate);
  }

  deleteCat(idCat : number) : Observable<any> {
    return this.httpClient.delete<any>('http://localhost:8080/api/cat/'+idCat);
  }
}
