import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {CatComponent} from "./components/cat/cat.component";

const routes: Routes = [
  {path: '', component: HomeComponent, title: 'Home'},
  {path: 'cat', component: CatComponent, title: 'Cat Component'},
  {path: 'cat-test/:id', component: HomeComponent, title: 'Cat Component'},
  {path: 'cat/:id', component: CatComponent, title: 'Cat test'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
