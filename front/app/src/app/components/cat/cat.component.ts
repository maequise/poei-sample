import {Component, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CatService} from "../../services/cat.service";
import {CatDto} from "../../cat-dto";
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-cat',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, RouterLink],
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.css']
})
export class CatComponent implements OnInit {
  cats : CatDto[] = [];
  catSelected : CatDto | undefined | null = null;
  form : FormGroup = this.fb.group({
    name : new FormControl<string>(''),
    age : new FormControl<number>(0),
    id : new FormControl<number>(0)
  });


  constructor(private catService : CatService, private fb : FormBuilder) {

  }

  ngOnInit(): void {
    this.catService.getAllCats().subscribe(resp => this.cats = resp);
  }

  selectCat(cat : CatDto) : void {
    console.log('cat selected : ' + cat.name);

    this.catSelected = cat;
  }

  update() {
    let catToUpdate = this.form.value;

    catToUpdate.id = this.catSelected.id;

    console.log(catToUpdate);

    this.catService.updateCatByEntity(catToUpdate).subscribe(r => console.log(r));
  }

  delete() {
    console.log('delete call')

      let id = this.catSelected?.id!;

      this.catService.deleteCat(id).subscribe(r => console.log(r));

  }

  add() {
    let cat = this.form.value;

    console.log(cat);

    this.catService.addNewCat(cat).subscribe(r => this.cats.push(r));
  }
}
